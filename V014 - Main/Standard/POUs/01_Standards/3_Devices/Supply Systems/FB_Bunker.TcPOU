﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.3">
  <POU Name="FB_Bunker" Id="{f6afe619-2b0f-4cc5-ba20-9804fbd5064f}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_Bunker EXTENDS FB_DeviceBase
VAR_INPUT
END_VAR
VAR_OUTPUT
END_VAR
VAR
	
	Q_Bunker	 AT %Q*	: BOOL;
	I_Sensor	 AT %I*	: BOOL;
	
	bInterlock: BOOL;
	
	Vis		: ST_BunkerVis;
	
	
	bHoldOnSensor 	: BOOL;
	bTransport		: BOOL;
	bSupply			: BOOL;

	bTimerDone		: BOOL;
	bDone			: BOOL;
	T_Timer			: TON;
	
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[// Visualisation
Vis.bStateSensor:= I_Sensor;
Vis.bStateSensorUsage:= bHoldOnSensor OR Vis.btnUseSensor;
Vis.bManualMode:= DeviceData.bManualMode;
Vis.bError:= DeviceData.bError;


// Manual operation
IF DeviceData.bManualMode AND Vis.btnTransport AND Vis.btnUseSensor THEN
		bTransport:= TRUE;
		bHoldOnSensor:= TRUE;
		IF bDone THEN
			Vis.btnTransport:= FALSE;
			bTransport:= FALSE;
			bHoldOnSensor:= FALSE;
		END_IF
ELSIF DeviceData.bManualMode AND Vis.btnTransport AND NOT Vis.btnUseSensor THEN
		bTransport:= TRUE;
		bHoldOnSensor:= FALSE;
ELSIF DeviceData.bManualMode AND Vis.btnSupply THEN
		bSupply:= TRUE;
		IF bDone THEN
			Vis.btnSupply:= FALSE;
			bSupply:= FALSE;
		END_IF
ELSE
	Vis.btnTransport:= FALSE;
	Vis.btnSupply:= FALSE;
END_IF


// Automatic operation
bDone:= FALSE;

// Transport /////////////////////////////////////////////////////////////////////
IF bHoldOnSensor AND bTransport THEN
	IF NOT I_Sensor THEN
		iErrorState:= 1;
		Q_Bunker:= TRUE;
	ELSE
		iErrorState:= 0;
		Q_Bunker:= FALSE;
		bDone := TRUE;
	END_IF
ELSIF NOT bHoldOnSensor AND bTransport THEN
	Q_Bunker:= TRUE;
	bDone := TRUE;
ELSIF NOT bTransport AND NOT bSupply THEN
	Q_Bunker:= FALSE;
END_IF

// Supply /////////////////////////////////////////////////////////////////////
IF bSupply AND NOT bTimerDone THEN
	Q_Bunker:= TRUE;
ELSIF bSupply AND bTimerDone THEN
	Q_Bunker:= FALSE;
	bDone:= TRUE;
ELSIF NOT bTransport AND NOT bSupply THEN
	Q_Bunker:= FALSE;
END_IF

T_Timer(IN:= bSupply AND NOT bTimerDone, PT:= UINT_TO_TIME(Vis.iSupplyTime));

// Take over timer output to prevent overflow
IF T_Timer.Q AND bSupply THEN
	bTimerDone:= TRUE;
ELSIF NOT bSupply THEN
	bTimerDone:= FALSE;
END_IF

Vis.bStateTransport:= Q_Bunker AND bTransport;
Vis.bStateSupply:= Q_Bunker AND bSupply;
bTransport:= FALSE;
bSupply:= FALSE;

// Stop hopper on error
IF DeviceData.bError THEN
	Q_Bunker:= FALSE;
END_IF]]></ST>
    </Implementation>
    <Method Name="FB_Init" Id="{5e283263-b494-4dad-839a-55d5d8994e39}">
      <Declaration><![CDATA[METHOD FB_init : BOOL
VAR_INPUT
	bInitRetains : BOOL; // if TRUE, the retain variables are initialized (warm start / cold start)
	bInCopyCode : BOOL;  // if TRUE, the instance afterwards gets moved into the copy code (online change)

	iDelayErrorTime: UINT;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[iErrorTime:= iDelayErrorTime;]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Abort" Id="{3d156ca9-4078-4ee4-b9f3-be83bf77e6b9}">
      <Declaration><![CDATA[{warning 'add method implementation '}
METHOD M_Abort : BOOL
]]></Declaration>
      <Implementation>
        <ST><![CDATA[bTransport:= FALSE;
bSupply:= FALSE;

M_Abort:= NOT (bTransport AND bSupply);]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Supply" Id="{1505328d-f879-4426-8e74-36f921d7e34a}">
      <Declaration><![CDATA[METHOD M_Supply : BOOL
VAR_INPUT
	
END_VAR
VAR
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[

IF bInterlock THEN
	bSupply:= TRUE;
ELSE
	bSupply:= FALSE;
END_IF

M_Supply:= bDone;]]></ST>
      </Implementation>
    </Method>
    <Method Name="M_Transport" Id="{9619421b-96f2-49b9-b069-9f1ea1769c9a}">
      <Declaration><![CDATA[METHOD M_Transport : BOOL
VAR_INPUT
	
	bHoldOnSensor : BOOL;
	
END_VAR

VAR
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[
IF bInterlock THEN
	bTransport:= TRUE;
ELSE
	bTransport:= FALSE;
END_IF;

M_Transport:= bDone;
]]></ST>
      </Implementation>
    </Method>
    <Property Name="PR_Interlock" Id="{d37da39e-7a6c-4371-94f2-2561d682858f}">
      <Declaration><![CDATA[{warning 'add property implementation'}
PROPERTY PR_Interlock : BOOL
]]></Declaration>
      <Get Name="Get" Id="{f82cbe18-8341-47ba-bac9-d11f6efb0373}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[]]></ST>
        </Implementation>
      </Get>
      <Set Name="Set" Id="{2b8203b8-a282-40b8-8007-f0cd664a7eba}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[bInterlock:= PR_Interlock;]]></ST>
        </Implementation>
      </Set>
    </Property>
    <Property Name="PR_Sensor" Id="{9e8a0547-08ce-4e49-a3bb-7303a73b637e}">
      <Declaration><![CDATA[PROPERTY PR_Sensor : BOOL]]></Declaration>
      <Get Name="Get" Id="{2e3fb4b6-3b35-4646-af57-9158eed18ab7}">
        <Declaration><![CDATA[VAR
END_VAR
]]></Declaration>
        <Implementation>
          <ST><![CDATA[PR_Sensor:= I_Sensor;]]></ST>
        </Implementation>
      </Get>
    </Property>
    <LineIds Name="FB_Bunker">
      <LineId Id="256" Count="77" />
      <LineId Id="173" Count="0" />
    </LineIds>
    <LineIds Name="FB_Bunker.FB_Init">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_Bunker.M_Abort">
      <LineId Id="4" Count="1" />
      <LineId Id="7" Count="0" />
      <LineId Id="6" Count="0" />
    </LineIds>
    <LineIds Name="FB_Bunker.M_Supply">
      <LineId Id="5" Count="0" />
      <LineId Id="14" Count="1" />
      <LineId Id="18" Count="0" />
      <LineId Id="21" Count="1" />
      <LineId Id="19" Count="0" />
      <LineId Id="17" Count="0" />
      <LineId Id="13" Count="0" />
    </LineIds>
    <LineIds Name="FB_Bunker.M_Transport">
      <LineId Id="25" Count="0" />
      <LineId Id="27" Count="3" />
      <LineId Id="20" Count="0" />
      <LineId Id="31" Count="0" />
      <LineId Id="5" Count="0" />
      <LineId Id="23" Count="0" />
    </LineIds>
    <LineIds Name="FB_Bunker.PR_Interlock.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_Bunker.PR_Interlock.Set">
      <LineId Id="2" Count="0" />
    </LineIds>
    <LineIds Name="FB_Bunker.PR_Sensor.Get">
      <LineId Id="2" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>